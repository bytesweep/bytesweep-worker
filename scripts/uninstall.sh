#!/bin/bash
tool_name="bytesweep-worker"

# if this bytesweep tool is the last one on the system, delete the bytesweep user and homedir
ls -l /opt/ | awk '{print $9}' | grep '^bytesweep-' | grep -q -v "^$tool_name\$"
if [  $? -ne 0 ] ; then
	userdel bytesweep
	rm -rf /home/bytesweep
	# only remove extraction_dir if all other bytesweep tools are gone
	rm -rf /var/opt/bytesweep-extraction
	# only remove /etc/bytesweep/ if all other bytesweep tools are gone
	rm -rf /etc/bytesweep/
	# delete postgres database and user
	sudo -u postgres psql -U postgres -d postgres -c "DROP DATABASE bytesweep;"
	sudo -u postgres psql -U postgres -d postgres -c "DROP USER bsuser;"
fi

# remove /opt/bytesweep-worker/ because "yum remove" sometimes doesn't remove it
rm -rf /opt/bytesweep-worker/
