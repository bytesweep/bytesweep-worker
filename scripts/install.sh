#!/bin/bash

# create bytesweep user and group
getent passwd bytesweep >/dev/null || useradd -r --create-home -U -d /home/bytesweep -s /sbin/nologin -c "Bytesweep user" bytesweep

# create bytesweep upload_dir
mkdir -p /var/opt/bytesweep-extraction
chown -R bytesweep:bytesweep /var/opt/bytesweep-extraction

# create /etc/bytesweep
if [ ! -d /etc/bytesweep ]; then
	mkdir /etc/bytesweep
fi

# config example-config to config if it doesn't exist
if [ ! -f /etc/bytesweep/config.yaml ]; then
	cp /opt/bytesweep-worker/example-config.yaml /etc/bytesweep/config.yaml
	chown bytesweep:bytesweep /etc/bytesweep/config.yaml
	chmod 400 /etc/bytesweep/config.yaml
fi

# setup python3 on rh/centos
if [ ! -f /etc/profile.d/python3.sh ]; then
	echo 'source /opt/rh/rh-python36/enable' > /etc/profile.d/python3.sh
fi

source /opt/rh/rh-python36/enable

# install python requirements
pip3 install -r /opt/bytesweep-worker/requirements.txt

# setup gunicorn path in service script
python_path=$(whereis python3 | awk '{print $2}')
sed -i -e "s#PYTHON_PATH#$python_path#g" /etc/systemd/system/bytesweep-worker.service

sudo systemctl enable bytesweep-worker
sudo systemctl start bytesweep-worker
