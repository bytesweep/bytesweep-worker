#!/usr/bin/env python
from modules.fstree import *
from modules.file_enrichment import *
from modules.radare2 import *
from modules.extract_keys import *
from modules.strings import *
from modules.binwalk import *
from ruamel.yaml import YAML
import shutil
import sys
import os
import json
import uuid
import time

def perform_analysis(project_basedir, firmware_filename):

	# TODO: parse these from config file or job db entry
	config_file = open('/etc/bytesweep/config.yaml','r')
	yaml=YAML()
	config = yaml.load(config_file.read())
	config_file.close()
	extract_dir = config['extraction_dir']
	multiprocess_workers = config['multiprocess_workers']
	do_unsafe_libs = True
	do_extract_keys = True
	do_string_analysis = True
	string_analysis_threshold = 8
	final_json_payload = {}
	abs_extract_dir = os.path.abspath(extract_dir)
	while True:
		uuid_string = str(uuid.uuid4())
		analysis_basedir = os.path.join(abs_extract_dir, uuid_string)
		if not os.path.exists(analysis_basedir):
			os.makedirs(analysis_basedir)
			break

	# TODO: CLEAN UP......
	firmware_filepath = os.path.abspath(firmware_filename)
	new_firmware_filepath = os.path.join(analysis_basedir, os.path.basename(firmware_filepath))
	try:
		shutil.copyfile(firmware_filepath,new_firmware_filepath)
	except:
		final_json_payload['error'] = True
		return final_json_payload
	firmware_filepath = new_firmware_filepath

	#extracted_files = []
	fid = 0
	relpath = os.path.relpath(firmware_filepath, analysis_basedir)
	original_file = {'path':firmware_filepath,'relpath':relpath,'fid':fid,'parent_fid':fid}
	fid += 1

	# start the recusive extraction process
	start_extraction = time.time()
	print("START EXTRACTION")
	binwalk_extract(original_file,fid,0,analysis_basedir,[])
	# start recursive search here
	tfid = 0
	fstree = {'type':'dir','node':{'name':'root','path':analysis_basedir,'relpath':'.','fid':tfid,'parent_fid':tfid,'branch_path':[]},'files':[],'dirs':[]}
	tfid =+ 1
	build_fstree(fstree,analysis_basedir,tfid)
	extraction_time = (time.time() - start_extraction)
	print("END EXTRACTION")
	print("EXTRACTION TIME: "+str(extraction_time)+" seconds")

	#enrich fstree
	start_enrichment = time.time()
	print("START ENRICHMENT")
	for f in fstree_iterate(fstree):
		enrich_file(f['node'])
	enrichment_time = (time.time() - start_enrichment)
	print("END ENRICHMENT")
	print("ENRICHMENT TIME: "+str(enrichment_time)+" seconds")
	
	# run unsafe_libs module if enabled
	start_unsafe_libs = time.time()
	print("START UNSAFE LIB ANALYSIS")
	if do_unsafe_libs:
		#for extracted_file in extracted_files:
		process_unsafe_libs(list(fstree_iterate(fstree)),multiprocess_workers)
		#process_unsafe_libs(extracted_files,multiprocess_workers)
		#process_unsafe_libs([original_file],multiprocess_workers)
	unsafe_libs_time = (time.time() - start_unsafe_libs)
	print("END UNSAFE LIB ANALYSIS")
	print("UNSAFE LIB ANALYSIS TIME: "+str(unsafe_libs_time)+" seconds")
	
	
	# assemble json payload and print to stdout
	#final_json_payload['original_file'] = original_file
	#final_json_payload['extracted_files'] = extracted_files
	final_json_payload['fstree'] = fstree
	
	# key extraction module
	#if do_extract_keys:
		#final_json_payload['extracted_keys'] = extract_keys([original_file] + extracted_files)
		#final_json_payload['extracted_keys'] = extract_keys(list(fstree_iterate(fstree)))
	
	start_string_analysis = time.time()
	print("START STRING ANALYSIS")
	if do_string_analysis:
		#final_json_payload['string_analysis'] = string_analysis([original_file] + extracted_files)
		final_json_payload['string_analysis'] = string_analysis(list(fstree_iterate(fstree)),string_analysis_threshold)
	string_analysis_time = (time.time() - start_string_analysis)
	print("END STRING ANALYSIS")
	print("STRING ANALYSIS TIME: "+str(string_analysis_time)+" seconds")

	# compile analysis stats
	unique_components = []
	unique_keys = []
	unique_passwords = []
	for string in final_json_payload['string_analysis']:
		if string['type'] == 'program version' or string['type'] == 'library version':
			component = {'type':string['type'],'version':string['match']}
			if component not in unique_components:
				unique_components.append(component)
		elif string['type'] == 'crypto-keys':
			keyhash = hashlib.sha256(string['match'].encode('utf-8')).hexdigest()
			if keyhash not in unique_keys:
				unique_keys.append(keyhash)
		elif string['type'] == 'password':
			if string['match'] not in unique_passwords:
				unique_passwords.append(string['match'])

	stats_binaries = 0
	for f in fstree_iterate(fstree):
		if 'unsafe_functions' in f['node']:
			if len(f['node']['unsafe_functions']) > 0:
				stats_binaries += 1

	stats = {}
	stats['files'] = len(list(fstree_iterate(fstree)))
	stats['keys'] = len(unique_keys)
	stats['passwords'] = len(unique_passwords)
	stats['components'] = len(unique_components)
	stats['binaries'] = stats_binaries
	final_json_payload['stats'] = stats
	
	# add times
	final_json_payload['extraction_time'] = int(extraction_time)
	final_json_payload['unsafe_libs_time'] = int(unsafe_libs_time)
	final_json_payload['string_analysis_time'] = int(string_analysis_time)
	final_json_payload['analysis_basedir'] = analysis_basedir
	final_json_payload['project_basedir'] = project_basedir
	final_json_payload['error'] = False
	return final_json_payload

