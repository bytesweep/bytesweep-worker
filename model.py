#!/usr/bin/env python
import psycopg2
import json
from ruamel.yaml import YAML

def dbconnect():
	config_file = open('/etc/bytesweep/config.yaml','r')
	yaml=YAML()
	config = yaml.load(config_file.read())
	config_file.close()
	try:
		conn = psycopg2.connect(host=config['dbhost'],dbname=config['dbname'], user=config['dbuser'], password=config['dbpass'])
		return conn
	except:
		print('ERROR: unable to connect to the database')
		return False

def dbclose(conn):
	conn.close()


def init_tables():
	js = init_job_status_type()
	j = init_jobs_table()
	d = init_data_table()
	return js and j and d

def init_job_status_type():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	# TODO check for existance of enum
	cur.execute("CREATE TYPE job_status AS ENUM ('queued', 'processing', 'done')")
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

def init_jobs_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='jobs')")
	if not cur.fetchone()[0]:
		cur.execute("create table jobs (job_id serial NOT NULL primary key,job_name varchar(255) NOT NULL unique,job_status job_status NOT NULL,job_project_basedir text NOT NULL, job_filename text NOT NULL,job_filepath text NOT NULL,job_notes text)")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def init_data_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='data')")
	if not cur.fetchone()[0]:
		cur.execute("create table data (data_id serial not null primary key,job_id serial references jobs(job_id),data jsonb not null)")
		cur.execute("CREATE INDEX idxgin ON data USING gin (data);")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def put_job(job_name, job_project_basedir, job_filename, job_filepath, job_notes):
	conn = dbconnect()
	if not conn:
		return False
	if get_job_by_name(job_name):
		return False
	cur = conn.cursor()
	job_status = 'queued'
	cur.execute("insert into jobs (job_name,job_status,job_project_basedir,job_filename,job_filepath,job_notes) values (%s,%s,%s,%s,%s,%s) returning job_id", (job_name, job_status, job_project_basedir, job_filename, job_filepath, job_notes))
	job_id = cur.fetchone()[0]
	conn.commit()
	cur.close()
	dbclose(conn)
	return job_id

def put_data(job_id,data):
	conn = dbconnect()
	if not conn:
		return False
	if not get_job_by_id(job_id):
		return False
	cur = conn.cursor()
	cur.execute("insert into data (job_id,data) values (%s,%s) returning data_id", (str(job_id),json.dumps(data)))
	data_id = cur.fetchone()[0]
	conn.commit()
	cur.close()
	dbclose(conn)
	return data_id


def get_job_by_id(job_id):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select job_name,job_status,job_project_basedir,job_filename,job_filepath,job_notes from jobs where job_id=%s",(job_id,))
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return {'job_name':result[0],'job_id':job_id,'job_status':result[1],'job_project_basedir':result[2],'job_filename':result[3],'job_filepath':result[4],'job_notes':result[5]}
	else:
		return False

def get_job_by_name(job_name):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select job_id,job_status,job_project_basedir,job_filename,job_filepath,job_notes from jobs where job_name=%s",(job_name,))
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return {'job_name':job_name,'job_id':result[0],'job_status':result[1],'job_project_basedir':result[2],'job_filename':result[3],'job_filepath':result[4],'job_notes':result[5]}
	else:
		return False

def get_queued_jobs():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select job_name,job_id,job_status,job_project_basedir,job_filename,job_filepath,job_notes from jobs where job_status=%s",('queued',))
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		new_jobs = []
		for result in results:
			new_jobs.append({'job_name':result[0],'job_id':result[1],'job_status':result[2],'job_project_basedir':result[3],'job_filename':result[4],'job_filepath':result[5],'job_notes':result[6]})
		return new_jobs
	else:
		return False

def get_processing_jobs():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select job_name,job_id,job_status,job_project_basedir,job_filename,job_filepath,job_notes from jobs where job_status=%s",('processing',))
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		new_jobs = []
		for result in results:
			new_jobs.append({'job_name':result[0],'job_id':result[1],'job_status':result[2],'job_project_basedir':result[3],'job_filename':result[4],'job_filepath':result[5],'job_notes':result[6]})
		return new_jobs
	else:
		return False

def get_jobs():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select job_name,job_id,job_status,job_project_basedir,job_filename,job_filepath,job_notes from jobs")
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		jobs = []
		for result in results:
			jobs.append({'job_name':result[0],'job_id':result[1],'job_status':result[2],'job_project_basedir':result[3],'job_filename':result[4],'job_filepath':result[5],'job_notes':result[6]})
		return jobs
	else:
		return False

def get_data(job_id):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select jobs.job_name,jobs.job_status,jobs.job_project_basedir,jobs.job_filename,jobs.job_filepath,jobs.job_notes,data.data from data join jobs on data.job_id=jobs.job_id where jobs.job_id=%s",(str(job_id),))
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return {'job_name':result[0],'job_status':result[1],'job_project_basedir':result[2],'job_filename':result[3],'job_filepath':result[4],'job_notes':result[5],'data':result[6]}
	else:
		return False
	
def delete_job_and_data(job_id):
	# delete data then job
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("delete from data where job_id=%s",(job_id,))
	cur.execute("delete from jobs where job_id=%s",(job_id,))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

def drop_tables():
	# delete data then job
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("DROP TABLE data")
	cur.execute("DROP TABLE jobs")
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

def drop_enums():
	# delete data then job
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("DROP TYPE job_status")
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

def drop_tables_and_enums():
	dt = drop_tables()
	de = drop_enums()
	return dt and de

def set_job_status(job_id, job_status):
	job_id = str(job_id)
	valid_job_status = ['queued', 'processing', 'done']
	if job_status not in valid_job_status:
		return False
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("UPDATE jobs SET job_status=%s WHERE job_id=%s", (job_status, job_id))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True
