#!/usr/bin/env python3
import r2pipe
import json
from modules.file_enrichment import *
import multiprocessing as mp

unsafe_function_list = ['sym.imp.gets','sym.imp.scanf','sym.imp.strcpy','sym.imp.strcat','sym.imp.sprintf','sym.imp.vsprintf','sym.imp.system','sym.imp.popen']

# this is a hack for binaries where radare2 doesn't correctly calculate libc function offsets
# TODO: make this a bit intelligent
offset = 0

def unsafe_libs(f,shared_dict):
	# ensure path is a file
	if not f['isfile'] or f['islink']:
		return

	# ensure file is a binary executable
	if not is_executable(f):
		return

	# only create a list if the path is a binary executable
	unsafe_functions = []

	# DEBUG
	#if '/usr/sbin/httpd' not in f:
	#	continue

	# open executable binary and perform anaylsis
	r2 = r2pipe.open(f['path'])
	# radare2's analysis can be noisy...
	r2.cmd('aaa 2> /dev/null')
	
	# get list of functions
	r2_anal_functions = json.loads(r2.cmd('aflj 2> /dev/null'))
	for r2_function in r2_anal_functions:
		if r2_function['name'] in unsafe_function_list:
			if 'codexrefs' not in r2_function:
				continue
			for xref in r2_function['codexrefs']:
				xref_address = hex(xref['addr'])
				library_address = hex(xref['at'])
				xref_info = json.loads(r2.cmd('pdj 1 @ '+xref_address+' 2> /dev/null'))[0]

				# attempt to resolve parent function name
				if xref_info['type'] == 'invalid':
					continue
				parent_fcn_address = hex(xref_info['fcn_addr'])
				parent_fcn_name = json.loads(r2.cmd('afij '+parent_fcn_address+' 2> /dev/null'))
				if len(parent_fcn_name) > 0:
					parent_fcn_name = parent_fcn_name[0]['name']
				else:
					parent_fcn_name = 'unknown'

				asm = xref_info['disasm']
				unsafe_fcn = {'fcn_name':r2_function['name'],'parent_fcn':parent_fcn_name,'xref_address':xref_address,'library_address':library_address,'type':xref['type'],'asm':asm}
				unsafe_functions.append(unsafe_fcn)
	
	r2.quit()
	shared_dict[f['fid']] = unsafe_functions


def process_unsafe_libs(files,multiprocess_workers):

	manager = mp.Manager()
	shared_dict = manager.dict()
	
	workers = []
	for i in range(multiprocess_workers):
		workers.append({'worker':False,'isrunning':False})
	
	
	i = 0
	while i < len(files):
		for worker in workers:
			if not worker['isrunning'] and i < len(files):
				worker['worker'] = mp.Process(target=unsafe_libs, args=(files[i]['node'],shared_dict))
				worker['worker'].start()
				worker['isrunning'] = True
				i += 1
			elif worker['worker']:
				if not worker['worker'].is_alive():
					worker['isrunning'] = False
	
	for worker in workers:
		if worker['worker']:
			worker['worker'].join()

	for f in files:
		if f['node']['fid'] in shared_dict:
			f['node']['unsafe_functions'] = shared_dict[f['node']['fid']]
