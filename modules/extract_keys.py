#!/usr/bin/env python3

def extract_keys(extracted_files):
	extracted_keys = []
	for extracted_file in extracted_files:
		if extracted_file['islink'] or extracted_file['isdir'] or not extracted_file['isfile']:
			continue
		for signature_result in extracted_file['signature_results']:
			if signature_result['description'] == 'PEM RSA private key':
				extracted_key = {'path':extracted_file['path'],'relpath':extracted_file['relpath'],'fid':extracted_file['fid'],'offset':signature_result['offset']}
				extracted_key['content'] = get_pemkey(extracted_file['path'],signature_result['offset'])
				if extracted_key['content'] != '':
					extracted_keys.append(extracted_key)
			if signature_result['description'] == 'PEM certificate':
				extracted_key = {'path':extracted_file['relpath'],'relpath':extracted_file['relpath'],'fid':extracted_file['fid'],'offset':signature_result['offset']}
				extracted_key['content'] = get_pemcert(extracted_file['path'],signature_result['offset'])
				if extracted_key['content'] != '':
					extracted_keys.append(extracted_key)
				


	return extracted_keys

def get_pemcert(filename,offset):
	pem = ''
	f = open(filename,'rb')
	f.seek(offset)
	while True:
		c = f.read(1)
		if not c:
			break
		pem += c.decode('utf-8')
		if pem.endswith('-----END CERTIFICATE-----'):
			if pem.startswith('-----BEGIN CERTIFICATE-----'):
				return pem
			else:
				return ''
	return ''

def get_pemkey(filename,offset):
	pem = ''
	f = open(filename,'rb')
	f.seek(offset)
	while True:
		c = f.read(1)
		if not c:
			break
		pem += c.decode('utf-8')
		if pem.endswith('-----END RSA PRIVATE KEY-----'):
			if pem.startswith('-----BEGIN RSA PRIVATE KEY-----'):
				return pem
			else:
				return ''
	return ''
