#!/usr/bin/env python
from modules.file_enrichment import enrich_file
import binwalk
import os

def binwalk_extract(f,fid,recursion_num,analysis_basedir,extracted_files):
	# STAND BACK! I'm going to do recursion!
	max_recursions = 5
	# set original file using same format as extracted_files
	# this will simplify the enriching process
	# scan file using binwalk

	enrich_file(f)
	if not f['isregfile'] or f['islink'] or f['isdir']:
		return

	try:
		scan = binwalk.scan(f['path'], signature=True, quiet=True, extract=True, directory=analysis_basedir)
	except binwalk.ModuleException as e:
		# this doesn't make sense here anymore
		# need to return some sort of error instead of exiting
		print("ERROR: binwalk.scan failed")
		return fid
	for module in scan:
		if module.name == 'Signature':
			signature_results = []
			f['signature_results'] = []
			for result in module.results:
	
				f['signature_results'].append({'offset':result.offset,'description':result.description})
				# if a file was carved out
				if result.file.path in module.extractor.output:
					# These are files/directories created by extraction utilities (gunzip, tar, unsquashfs, etc)
					if result.offset in module.extractor.output[result.file.path].extracted:
						for path in module.extractor.output[result.file.path].extracted[result.offset].files:
							if os.path.isfile(path):
								# this is to fill in the gap of dirs that don't end up in the filelist. might need to do more...
								if f['fid'] == 0:
									pathdir = os.path.dirname(path)
									relpath = os.path.relpath(pathdir,analysis_basedir)
									new_f = {'path':pathdir,'relpath':relpath,'fid':fid,'parent_fid':f['fid']}
									fid += 1
									extracted_files.append(new_f)
									if recursion_num <= max_recursions:
										binwalk_extract(new_f,fid,recursion_num+1,analysis_basedir,extracted_files)
								relpath = os.path.relpath(path, analysis_basedir)
								new_f = {'path':path,'relpath':relpath,'fid':fid,'parent_fid':f['fid']}
								fid += 1
								extracted_files.append(new_f)
								if recursion_num <= max_recursions:
									binwalk_extract(new_f,fid,recursion_num+1,analysis_basedir,extracted_files)
							elif os.path.isdir(path):
								for root,dirs,files in os.walk(path):
									for name in files + dirs:
										path = os.path.abspath(os.path.join(root, name))
										relpath = os.path.relpath(path,analysis_basedir)
										new_f = {'path':path,'relpath':relpath,'fid':fid,'parent_fid':f['fid']}
										fid += 1
										extracted_files.append(new_f)
										if recursion_num <= max_recursions:
											binwalk_extract(new_f,fid,recursion_num+1,analysis_basedir,extracted_files)

					# These are files that binwalk carved out of the original firmware image, a la dd
					elif result.offset in module.extractor.output[result.file.path].carved:
						# TODO: put this into extracted_files
						pass
	return fid
