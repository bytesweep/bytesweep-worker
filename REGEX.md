# ByteSweep Regex Search System

ByteSweep employs a regex-based search system for identifying static string content of interest.

## YAML config file

bytesweep-worker uses `/etc/bytesweep/regex.yaml` as the config file for the regex search system.

### String Types

- `program version`: binary executable version string
- `library version`: shared library object version string
- `crypto-keys`: cryptographic key string
- `password`: password hash string

### regex search rule formats

#### Standard

A standard regex search that takes a `string` and searches for matches.

Example:
```
 - regex:
    string: 'Enabled GnuTLS [0-9]+\.[0-9]+\.[0-9]+ logging'
    name: 'gnutls'
    type: 'library version'
    description: 'gnutls version'
    left_nibble: 15
    right_nibble: 8
```

Field Desciptions:
- `string`: regular expression
- `name`: identifier for the static string data
- `type`: see `String Types` above
- `description`: description of the regex search rule
- `left_nibble`: number of characters to remove from left side of the matched string
- `right_nibble`: number of characters to remove from right side of the matched string

#### Nearby

A regex search that looks for an initial regex match, and then searches for a second regex match nearby.

Example:
```
 - regex:
    string: 'General [a-zA-Z\s]*Options:'
    name: 'openvpn'
    type: 'program version'
    description: 'openvpn version'
    nearby:
       string: 'OpenVPN [0-9]+\.[0-9]+\.[0-9]+ '
       name: 'openvpn'
       type: 'program version'
       description: 'openvpn version'
       search_above: 5
       search_below: 5
       left_nibble: 8
       right_nibble: 1
```

Field Desciptions:
`search_above`: number of strings prior to the first regex match to search for the second regex match.
`search_below`: number of strings after the first regex match to search for the second regex match.

#### Multiline

A multiline regex search

Example:
```
 - regex:
    string: '-----BEGIN RSA PRIVATE KEY-----'
    name: 'rsa-private-key'
    type: 'crypto-keys'
    description: 'RSA Private Key'
    multiline:
       string: '-----BEGIN RSA PRIVATE KEY-----[a-zA-Z0-9+=/\n]{20,}?-----END RSA PRIVATE KEY-----'
       name: 'rsa-private-key'
       type: 'crypto-keys'
       description: 'RSA Private Key'
       search_above: 0
       search_below: 50
       left_nibble: 0
       right_nibble: 0
```

