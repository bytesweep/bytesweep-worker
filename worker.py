#!/usr/bin/env python3
from model import get_queued_jobs, get_processing_jobs, set_job_status, put_data
from analysis import perform_analysis
import time
import multiprocessing as mp

if __name__ == '__main__':
	# set multiprocessing start method
	mp.set_start_method('forkserver')
	# reset jobs that are processing...
	processing_jobs = get_processing_jobs()
	if processing_jobs:
		for job in processing_jobs:
			set_job_status(job['job_id'] ,'queued')
	
	
	while True:
		new_jobs = get_queued_jobs()
		time.sleep(5)
	
		if new_jobs:
			for new_job in new_jobs:
				set_job_status(new_job['job_id'] ,'processing')
				analysis_results = perform_analysis(new_job['job_project_basedir'], new_job['job_filepath'])
				if analysis_results:
					put_data(new_job['job_id'], analysis_results)
					set_job_status(new_job['job_id'] ,'done')
				else:
					print('ERROR!!!!')

